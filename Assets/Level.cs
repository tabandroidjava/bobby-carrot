﻿using System;


namespace BobbyCarrot
{
	[Serializable]
	public struct Level
	{
		public int[][] bottomArray, middleArray, topArray;
	}
}