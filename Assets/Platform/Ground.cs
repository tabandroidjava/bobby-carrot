﻿using BobbyCarrot.Movers;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


namespace BobbyCarrot.Platforms
{
	public sealed class Ground : Platform
	{
		public enum Name
		{
			Normal, Start, End
		}

		private Name Δname;
		public new Name name
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => Δname;

			private set
			{
				Δname = value;
				switch (value)
				{
					case Name.Normal: break;

					case Name.Start: startPoint = this; break;

					case Name.End: endPoint = this; break;
				}
			}
		}

		public static Ground startPoint { get; private set; }

		public static Ground endPoint { get; private set; }


		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void _() { }

		static Ground()
		{
			var list = new List<int> { 149, 150, 158 };
			for (int i = 94; i <= 147; ++i) list.Add(i);
			(dict as Dictionary<List<int>, Func<int, Vector3Int, Platform>>)
				[list] = (id, position) =>
				{
					var ground = Instantiate(Prefab.Load<Ground>(), position, Quaternion.identity);
					switch (id)
					{
						case 149: ground.name = Name.Start; break;

						case 150: ground.name = Name.End; break;

						default: ground.name = Name.Normal; break;
					}
					return ground;
				};
		}


		//public override bool CanEnter(Mover mover)
		//{
		//	throw new NotImplementedException();
		//}


		//public override void OnEnter(Mover mover)
		//{
		//	throw new NotImplementedException();
		//}
	}
}