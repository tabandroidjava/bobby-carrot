﻿using UnityEngine;


namespace BobbyCarrot.Platforms
{
	/// <summary>
	/// Cung cấp sprites[id] cho <see cref="Platform"/>
	/// </summary>
	[CreateAssetMenu(menuName = "Platform Sprites")]
	internal sealed class PlatformSprites : ScriptableObject
	{
		public Sprite[] sprites;
	}
}