﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace BobbyCarrot.Platforms
{
	public sealed class Obstacle : Platform
	{
		public enum Name
		{
			Normal, Sky, Water
		}
		public new Name name { get; private set; }


		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void _() { }

		static Obstacle()
		{
			var list = new List<int> { 91 };
			for (int i = 0; i <= 76; ++i) list.Add(i);
			for (int i = 78; i <= 86; ++i) list.Add(i);
			for (int i = 249; i <= 254; ++i) list.Add(i);
			(dict as Dictionary<List<int>, Func<int, Vector3Int, Platform>>)
				[list] = (id, position) =>
				  {
					  var obstacle = Instantiate(Prefab.Load<Obstacle>(), position, Quaternion.identity);
					  if (71 <= id && id <= 76)
					  {
						  obstacle.name = Name.Sky;
						  if (id == 72) /*obstacle.animator.runtimeAnimatorController = R.asset.anim.star;*/
							  throw new NotImplementedException();
					  }
					  else if (id == 85 || id == 86 || id == 91)
					  {
						  obstacle.name = Name.Water;
						  if (id == 86)
							  /*obstacle.animator.runtimeAnimatorController = R.asset.anim.water;*/
							  throw new NotImplementedException();
						  else if (id == 91) /*obstacle.animator.runtimeAnimatorController = R.asset.anim.waterFall;*/
							  throw new NotImplementedException();
					  }
					  else
					  {
						  obstacle.name = Name.Normal;
						  if (249 <= id && id <= 254) obstacle.spriteRenderer.sortingLayerName = "Middle Platform";
					  }
					  return obstacle;
				  };
		}
	}
}