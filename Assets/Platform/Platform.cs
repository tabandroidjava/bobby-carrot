﻿using BobbyCarrot.Movers;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
#if DEBUG
using System.Linq;
#endif
using UnityEngine;


namespace BobbyCarrot.Platforms
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(SpriteRenderer))]
	public abstract class Platform : MonoBehaviour, IPlatform
	{
		[SerializeField] protected SpriteRenderer spriteRenderer;

		public static ReadOnlyArray<ReadOnlyArray<Stack<IPlatform>>> array { get; private set; }



		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void _()
		{

		}



		static Platform()
		{
			Board.onReset += () =>
			  {
				  var rect = Board.rect;
				  var array = new ReadOnlyArray<Stack<IPlatform>>[rect.width];
				  for (int x = 0; x < rect.width; ++x)
				  {
					  var column = new Stack<IPlatform>[rect.height];
					  for (int y = 0; y < rect.height; ++y) column[y] = new Stack<IPlatform>();
					  array[x] = new ReadOnlyArray<Stack<IPlatform>>(column);
				  }
				  Platform.array = new ReadOnlyArray<ReadOnlyArray<Stack<IPlatform>>>(array);
			  };
		}







		protected static readonly IReadOnlyDictionary<List<int>, Func<int, Vector3Int, Platform>> dict = new Dictionary<List<int>, Func<int, Vector3Int, Platform>>();

		public static Platform New(int id, Vector3Int position)
		{
			foreach (var list_func in dict)
				if (list_func.Key.Contains(id))
				{
					var platform = list_func.Value(id, position);
					platform.spriteRenderer.sprite = Prefab.Load<PlatformSprites>().sprites[id];
					platform.transform.parent = Board.platformAnchor;
					array[position.x][position.y].Push(platform);
					return platform;
				}

			throw new ArgumentOutOfRangeException();
		}





		public virtual bool CanExit(Mover mover) => true;


		public virtual bool CanEnter(Mover mover) => true;


		public virtual void OnExit(Mover mover) { }


		public virtual void OnEnter(Mover mover) { }


#if DEBUG
		public static void CheckValidDict()
		{
			foreach (var a in dict.Keys)
				foreach (var b in dict.Keys)
					if (a != b && a.Intersect(b).Any()) throw new Exception();
		}
#endif
	}



	public interface IPlatform
	{
		bool CanExit(Mover mover);

		bool CanEnter(Mover mover);

		void OnExit(Mover mover);

		void OnEnter(Mover mover);
	}



	public interface IButton
	{
		void ChangeState();
	}
}