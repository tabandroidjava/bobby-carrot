﻿using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;


namespace BobbyCarrot
{
	public enum Direction
	{
		Up, Right, Down, Left
	}



	public static class Extensions
	{
		public static async UniTask Move(this Transform transform, Vector3 dest, float speed, CancellationToken token = default, int delay = 0)
		{
			token.ThrowIfCancel_Optimized();
			while (transform.position != dest)
			{
				transform.position = Vector3.MoveTowards(transform.position, dest, speed);
				await UniTask.Delay(delay, cancellationToken: token);
			}
			transform.position = dest;
		}


		/// <summary>
		/// Kiểm tra tất cả trường hợp kể cả point nằm trên cạnh của <see cref="Rect"/>
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool TrueContains(this RectInt rect, Vector2Int point) =>
			point.x >= rect.xMin && point.x <= rect.xMax && point.y >= rect.yMin && point.y <= rect.yMax;


		private static readonly OperationCanceledException cachedOperationCanceledException = new OperationCanceledException("DONOT USE THIS EXCEPTION INFORMATION ! IT IS CACHED !");
		public static void ThrowIfCancel_Optimized(this CancellationToken token)
		{
			if (!token.IsCancellationRequested) return;
			throw
#if DEBUG
				new OperationCanceledException(token);
#else
				cachedOperationCanceledException;
#endif
		}


		/// <summary>
		/// <c> <paramref name="v"/>.z = 0 </c>
		/// </summary>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static void Set(this Vector3 v, float newX, float newY) => v.Set(newX, newY, 0);


		#region UniTask
		/// <summary>
		/// Bỏ qua <see cref="OperationCanceledException"/>
		/// </summary>
		public static async UniTaskVoid CheckException(this UniTask task)
		{
			while (task.Status == UniTaskStatus.Pending) await UniTask.Yield();
			if (task.Status == UniTaskStatus.Faulted) await task;
		}


		/// <summary>
		/// Bỏ qua <see cref="OperationCanceledException"/>
		/// </summary>
		public static async UniTaskVoid CheckException<T>(this UniTask<T> task)
		{
			while (task.Status == UniTaskStatus.Pending) await UniTask.Yield();
			if (task.Status == UniTaskStatus.Faulted) await task;
		}


		#region Track UniTask
		private static readonly Dictionary<string, UniTask> tasks = new Dictionary<string, UniTask>();


		/// <summary>
		/// Lưu vết (cache) <paramref name="task"/> tương ứng <paramref name="key"/> và kiểm ra Exception
		/// </summary>
		/// <exception cref="InvalidOperationException"><paramref name="key"/> có task đang chạy</exception>
		public static void Track(this UniTask task, string key)
		{
			if (string.IsNullOrEmpty(key)) throw new ArgumentOutOfRangeException();

			if (tasks.TryGetValue(key, out UniTask value) && value.Status == UniTaskStatus.Pending)
				throw new InvalidOperationException($"Không thể track task= {task}, key= {key} vì có task khác đang chạy: {value} ");

			(tasks[key] = task).CheckException();
		}


		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static UniTask? GetTask(this string key) =>
			tasks.TryGetValue(key, out UniTask task) ? task : (UniTask?)null;
		#endregion


		#region Track UniTask<T>
		private static class GenericUniTaskDictionary<T>
		{
			public static readonly Dictionary<string, UniTask<T>> tasks = new Dictionary<string, UniTask<T>>();
		}


		/// <summary>
		/// Lưu vết (cache) <paramref name="task"/> tương ứng <paramref name="key"/> và kiểm ra Exception
		/// </summary>
		/// <exception cref="InvalidOperationException"><paramref name="key"/> có task đang chạy</exception>
		public static void Track<T>(this UniTask<T> task, string key)
		{
			if (string.IsNullOrEmpty(key)) throw new ArgumentOutOfRangeException();

			var tasks = GenericUniTaskDictionary<T>.tasks;
			if (tasks.TryGetValue(key, out UniTask<T> value) && value.Status == UniTaskStatus.Pending)
				throw new InvalidOperationException($"Không thể track task= {task}, key= {key} vì có task khác đang chạy: {value} ");

			(tasks[key] = task).CheckException();
		}


		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static UniTask<T>? GetTask<T>(this string key) =>
			GenericUniTaskDictionary<T>.tasks.TryGetValue(key, out UniTask<T> task) ? task : (UniTask<T>?)null;
		#endregion
		#endregion


		public static Vector2 ToUnitVector(this Direction direction)
		{
			switch (direction)
			{
				case Direction.Up: return Vector2.up;
				case Direction.Right: return Vector2.right;
				case Direction.Down: return Vector2.down;
				default: return Vector2.left;
			}
		}
	}
}