﻿using BobbyCarrot.Movers;
using BobbyCarrot.Platforms;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using System;
using UnityEngine;


namespace BobbyCarrot
{
	public sealed class Board : MonoBehaviour
	{
		public static event Action onReset, onStart;
		[SerializeField] private Transform m_platformAnchor, m_moverAnchor;
		public static Transform platformAnchor => instance.m_platformAnchor;
		public static Transform moverAnchor => instance.m_moverAnchor;
		[SerializeField] private Walker walker;


		private static Board instance;
		public TextAsset mapFile; // debug
		private static Level level;
		public static RectInt rect { get; private set; }

		private void Awake()
		{
			instance = instance ? throw new Exception() : this;
			onReset += () => { }; // debug
			onStart += () => { }; // debug
			level = JsonConvert.DeserializeObject<Level>(mapFile.text);
			rect = new RectInt(0, 0, level.middleArray.Length, level.middleArray[0].Length);
			onReset();
		}


		private async void Start()
		{
			bool hasBottom = level.bottomArray != null;
			var index = new Vector3Int();

			for (index.x = 0; index.x < rect.width; ++index.x)
				for (index.y = 0; index.y < rect.height; ++index.y)
				{
					int middleID = level.middleArray[index.x][index.y],
						topID = level.topArray[index.x][index.y];
					var (bottomID, moverID) =
						hasBottom ? (level.bottomArray[index.x][index.y], middleID) : (-1, topID);

					if (bottomID != -1) Platform.New(bottomID, index);
					if (middleID != -1) Platform.New(middleID, index);
					if (moverID != -1) Mover.New(moverID, index);
					if (topID != -1) Platform.New(topID, index);
				}

			await UniTask.Yield(PlayerLoopTiming.PreLateUpdate);
			onStart();
			Camera.main.transform.position = new Vector3(rect.width / 2f, rect.height / 2f, -10);
			walker.transform.position = Ground.startPoint.transform.position;
			walker.gameObject.SetActive(true);
		}
	}
}