﻿using Newtonsoft.Json;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;


namespace BobbyCarrot.Editor
{
	internal static class LevelGenerator
	{
		[MenuItem("Bobby Carrot/Generate Level")]
		private static void GenerateLevel()
		{
			var t = AssetDatabase.LoadAssetAtPath<Transform>("Assets/Editor/Prefab/Map 0.prefab");
			var top = t.GetChild(0).GetComponent<Tilemap>();
			if (top.name != "Top") throw new System.Exception();
			var middle = t.GetChild(1).GetComponent<Tilemap>();
			if (middle.name != "Middle") throw new System.Exception();
			var bottom = t.GetChild(2).GetComponent<Tilemap>();
			if (bottom.name != "Bottom") throw new System.Exception();

			top.CompressBounds();
			middle.CompressBounds();
			bottom.CompressBounds();
			top.origin = middle.origin = bottom.origin;
			var map = bottom.gameObject.activeSelf ? bottom : middle;
			var size = map.size;
			var origin = map.origin;

			using (var stringWriter = new StringWriter())
			using (var w = new JsonTextWriter(stringWriter))
			{
				w.WriteStartObject();
				w.WritePropertyName("bottomArray");
				if (bottom.gameObject.activeSelf) WriteArray(bottom); else w.WriteNull();
				
				w.WritePropertyName("middleArray");
				WriteArray(middle);

				w.WritePropertyName("topArray");
				WriteArray(top);
				w.WriteEndObject();
				AssetDatabase.CreateAsset(new TextAsset(stringWriter.ToString()), "Assets/Map 0.asset");
				AssetDatabase.SaveAssets();


				void WriteArray(Tilemap m)
				{
					var index = new Vector3Int();
					int x, y;
					w.WriteStartArray();
					for (x = 0, index.x = origin.x; x < size.x; ++x, ++index.x)
					{
						w.WriteStartArray();
						for (y = 0, index.y = origin.y; y < size.y; ++y, ++index.y)
						{
							var tile = m.GetTile(index);
							w.WriteValue(tile != null ? Convert.ToInt32(tile.name) : -1);
						}
						w.WriteEndArray();
					}
					w.WriteEndArray();
				}
			}

			Debug.Log("Done generating level !");
		}
	}
}