﻿using BobbyCarrot.Platforms;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


namespace BobbyCarrot.Movers
{
	[DisallowMultipleComponent]
	[RequireComponent(typeof(SpriteRenderer))]
	public abstract class Mover : MonoBehaviour
	{
		[SerializeField] protected SpriteRenderer spriteRenderer;


		protected static readonly IReadOnlyDictionary<List<int>, Func<int, Vector3Int, Mover>> dict = new Dictionary<List<int>, Func<int, Vector3Int, Mover>>();
		public static Mover New(int id, Vector3Int position)
		{
			foreach (var list_func in dict)
				if (list_func.Key.Contains(id)) return list_func.Value(id, position);

			throw new ArgumentOutOfRangeException();
		}


		[NonSerialized]
		public int moveDistance;
		public Direction? moveDirection;

		/// <returns><see langword="true"/> : có thể di chuyển được</returns>
		protected virtual async UniTask<bool> CheckPlatform()
		{
			bool canMove = false;
			while (moveDistance-- > 0)
			{
				var current = Vector2Int.FloorToInt(transform.position);
				var next = current + Vector2Int.FloorToInt(moveDirection.Value.ToUnitVector());

				if (Board.rect.TrueContains(next))
				{
					var currentPlatform = Platform.array[current.x][current.y].Peek();
					var nextPlatform = Platform.array[next.x][next.y].Peek();

					if (currentPlatform.CanExit(this) && nextPlatform.CanEnter(this))
					{
						enabled = false;
						await Move();
						enabled = true;
						currentPlatform.OnExit(this);
						nextPlatform.OnEnter(this);
						canMove = true;
						if (!enabled) break;
						continue;
					}
				}

				break;
			}

			return canMove;
		}


		protected abstract UniTask Move();


		// debug
		/// <summary>
		/// Gọi trong Update()
		/// </summary>
		protected static Direction? GetPlayerInput()
		{
			var k = Keyboard.current;
			return (k.upArrowKey.isPressed || k.wKey.isPressed) ? Direction.Up :
				(k.rightArrowKey.isPressed || k.dKey.isPressed) ? Direction.Right :
				(k.downArrowKey.isPressed || k.sKey.isPressed) ? Direction.Down :
				(k.leftArrowKey.isPressed || k.aKey.isPressed) ? Direction.Left : (Direction?)null;
		}
	}
}