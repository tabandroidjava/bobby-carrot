﻿//using Cysharp.Threading.Tasks;
//using RotaryHeart.Lib.SerializableDictionary;
//using System;
//using System.Collections.Generic;
//using System.Runtime.CompilerServices;
//using UnityEngine;
//using UnityEngine.InputSystem;


//namespace BobbyCarrot.Movers
//{
//	[DisallowMultipleComponent]
//	[RequireComponent(typeof(SpriteRenderer), typeof(Animator))]
//	public sealed class Bobby : Mover
//	{
//		private abstract class StateBase
//		{
//			[SerializeField] protected Bobby bobby;

//			public abstract void Awake();

//			/// <summary>
//			/// Không gọi trực tiếp ! Dùng <see cref="state"/> hoặc MonoBehaviour magic callback 
//			/// </summary>
//			public abstract void OnEnable();

//			/// <summary>
//			/// Không gọi trực tiếp ! Dùng <see cref="state"/> hoặc MonoBehaviour magic callback 
//			/// </summary>
//			public abstract void OnDisable();
//		}


//		private interface IUpdate
//		{
//			void Update();
//		}
//		private IUpdate listener;
//		[SerializeField] private Animator animator;
//		private readonly IReadOnlyDictionary<State, StateBase> states = new Dictionary<State, StateBase>();


//		public enum State
//		{
//			Normal, Flying, Mowing
//		}

//		private State? Δstate;
//		public State? state
//		{
//			[MethodImpl(MethodImplOptions.AggressiveInlining)]
//			get => Δstate;

//			set
//			{
//				if (value == Δstate) return;
//				if (value == null)
//				{
//					gameObject.SetActive(false);
//					return;
//				}

//				if (Δstate != null) states[Δstate.Value].OnDisable();
//				states[(Δstate = value).Value].OnEnable();
//			}
//		}


//		public Direction direction
//		{
//			get;
//			private set;
//		} = Direction.Down;



//		



//		[Serializable]
//		private sealed class Flying : StateBase
//		{
//			[SerializeField] private SerializableDictionaryBase<Direction, Sprite> sprites;
//			[SerializeField] private float speed;
//			[SerializeField] private int delayMilisec;


//			public override void Awake()
//			{

//			}


//			public override async void OnEnable()
//			{
//				bobby.animator.enabled = false;
//				bobby.spriteRenderer.sprite = sprites[bobby.direction];
//				//Vector3 v = bobby.direction.ToUnitVector() * speed;
//				//while (true)
//				//{
//				//	bobby.transform.position += v;
//				//	await UniTask.Delay(delayMilisec);
//				//}
//				throw new NotImplementedException();
//			}


//			public override void OnDisable()
//			{

//			}

//		}
//		[SerializeField] private Flying flying;



//		[Serializable]
//		private sealed class Mowing : StateBase, IUpdate
//		{
//			[SerializeField] private RuntimeAnimatorController controller;

//			private static readonly IReadOnlyDictionary<Direction, int> MOVES = new Dictionary<Direction, int>
//			{
//				[Direction.Up] = Animator.StringToHash("UP"),
//				[Direction.Right] = Animator.StringToHash("RIGHT"),
//				[Direction.Down] = Animator.StringToHash("DOWN"),
//				[Direction.Left] = Animator.StringToHash("LEFT")
//			};


//			public override void Awake()
//			{
//			}


//			public override void OnEnable()
//			{
//				bobby.listener = this;
//				bobby.animator.runtimeAnimatorController = controller;
//				bobby.animator.enabled = true;
//			}


//			public override void OnDisable()
//			{
//				bobby.listener = null;
//			}


//			public void Update()
//			{
//				var k = Keyboard.current;
//				var dir = k.leftArrowKey.isPressed ? Direction.Left :
//					k.rightArrowKey.isPressed ? Direction.Right :
//					k.upArrowKey.isPressed ? Direction.Up :
//					k.downArrowKey.isPressed ? Direction.Down : (Direction?)null;

//				if (dir != null)
//					bobby.animator.SetTrigger(MOVES[bobby.direction = dir.Value]);
//			}
//		}
//		[SerializeField] private Mowing mowing;








//		private static Bobby instance;
//		private void Awake()
//		{
//			instance = instance ? throw new Exception() : this;
//			var states = this.states as Dictionary<State, StateBase>;
//			states[State.Normal] = normal;
//			states[State.Flying] = flying;
//			states[State.Mowing] = mowing;
//		}


//		private void OnEnable()
//		{
//			direction = Direction.Down;
//			state = State.Normal;
//		}


//		private void OnDisable()
//		{
//			if (gameObject.activeSelf) throw new NotImplementedException();
//			if (Δstate == null) return;
//			var state = Δstate.Value;
//			Δstate = null;
//			states[state].OnDisable();
//		}


//		private void Update()
//		{
//			listener?.Update();
//		}


//		protected override UniTask Move()
//		{
//			throw new NotImplementedException();
//		}
//	}
//}