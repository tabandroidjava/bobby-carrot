﻿using BobbyCarrot.Platforms;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using UnityEngine;


namespace BobbyCarrot.Movers
{
	public sealed class LotusLeaf : Mover, IPlatform
	{
		public bool hasBobby { get; private set; }


		static LotusLeaf()
		{
			(dict as Dictionary<List<int>, Func<int, Vector3Int, Mover>>)
				[new List<int> { 236 }] = (id, position) =>
				  {
					  var lotus = Instantiate(Prefab.Load<LotusLeaf>(), position, Quaternion.identity);
					  lotus.transform.parent = Board.moverAnchor;
					  Platform.array[position.x][position.y].Push(lotus);
					  return lotus;
				  };
		}


		public bool CanEnter(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		public bool CanExit(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		public void OnEnter(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		public void OnExit(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		protected override UniTask Move()
		{
			throw new NotImplementedException();
		}
	}
}