﻿using BobbyCarrot.Platforms;
using Cysharp.Threading.Tasks;
using RotaryHeart.Lib.SerializableDictionary;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;


namespace BobbyCarrot.Movers
{
	public sealed class DynamicCloud : Mover, IPlatform
	{
		[SerializeField] private SerializableDictionaryBase<PinWheel.Color, Sprite> sprites;

		private PinWheel.Color Δcolor;
		public PinWheel.Color color
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => Δcolor;

			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			private set => spriteRenderer.sprite = sprites[Δcolor = value];
		}


		static DynamicCloud()
		{
			(dict as Dictionary<List<int>, Func<int, Vector3Int, Mover>>)
				[new List<int> { 224, 225, 226 }] = (id, position) =>
					 {
						 var cloud = Instantiate(Prefab.Load<DynamicCloud>(), position, Quaternion.identity);
						 switch (id)
						 {
							 case 224:
								 cloud.color = PinWheel.Color.Red;
								 break;

							 case 225:
								 cloud.color = PinWheel.Color.Violet;
								 break;

							 case 226:
								 cloud.color = PinWheel.Color.Green;
								 break;
						 }

						 cloud.transform.parent = Board.moverAnchor;
						 Platform.array[position.x][position.y].Push(cloud);
						 return cloud;
					 };
		}


		public bool CanEnter(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		public bool CanExit(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		public void OnEnter(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		public void OnExit(Mover mover)
		{
			throw new System.NotImplementedException();
		}


		protected override UniTask Move()
		{
			throw new NotImplementedException();
		}
	}
}