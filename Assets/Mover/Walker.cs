﻿using Cysharp.Threading.Tasks;
using RotaryHeart.Lib.SerializableDictionary;
using System.Collections.Generic;
using UnityEngine;


namespace BobbyCarrot.Movers
{
	public sealed class Walker : Mover
	{
		[SerializeField] private Animator animator;
		[SerializeField] private SerializableDictionaryBase<Direction, Sprite> idleSprites;
		[SerializeField] private int delayRelaxMilisec;

		private static readonly int DIE = Animator.StringToHash("DIE"),
			DISAPPEAR = Animator.StringToHash("DISAPPEAR"),
			RELAX = Animator.StringToHash("RELAX");
		private static readonly IReadOnlyDictionary<Direction, int> WALKS = new Dictionary<Direction, int>
		{
			[Direction.Up] = Animator.StringToHash("WALK UP"),
			[Direction.Right] = Animator.StringToHash("WALK RIGHT"),
			[Direction.Down] = Animator.StringToHash("WALK DOWN"),
			[Direction.Left] = Animator.StringToHash("WALK LEFT")
		},
		SCRATCHS = new Dictionary<Direction, int>
		{
			[Direction.Up] = Animator.StringToHash("SCRATCH UP"),
			[Direction.Right] = Animator.StringToHash("SCRATCH RIGHT"),
			[Direction.Down] = Animator.StringToHash("SCRATCH DOWN"),
			[Direction.Left] = Animator.StringToHash("SCRATCH LEFT")
		};


		private void Awake()
		{
		}


		private void OnEnable()
		{
			animator.enabled = false;
			spriteRenderer.sprite = idleSprites[Direction.Down];
			//cancelRelax.CancelAndReset();
			//WaitToRelax().CheckException();
		}


		private void OnDisable()
		{
			//cancelRelax.CancelAndReset();
		}


		private void Update()
		{
			moveDirection = moveDirection ?? GetPlayerInput();
			if (moveDirection != null) CheckPlatform().CheckException();
		}


		[SerializeField] private float speed;
		[SerializeField] private int delayMilisec;
		protected override async UniTask Move()
		{
			animator.SetTrigger(WALKS[moveDirection.Value]);
			animator.enabled = true;
			await transform.Move(transform.position + (Vector3)moveDirection.Value.ToUnitVector(), speed, delay: delayMilisec);
		}


		protected override async UniTask<bool> CheckPlatform()
		{
			bool canMove = await base.CheckPlatform();
			animator.enabled = false;
			spriteRenderer.sprite = idleSprites[moveDirection.Value];
			moveDirection = null;
			return canMove;
		}


		private readonly CustomCancelSource cancelRelax = new CustomCancelSource();
		private async UniTask WaitToRelax()
		{
			await UniTask.Delay(delayRelaxMilisec, cancellationToken: cancelRelax.Token);
			animator.enabled = true;
			animator.SetTrigger(RELAX);
		}
	}
}