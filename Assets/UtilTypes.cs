﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace BobbyCarrot
{
	public readonly struct ReadOnlyArray<T> : IEnumerable<T>
	{
		private readonly T[] array;


		public ReadOnlyArray(T[] array) => this.array = array;

		public T this[int index] => array[index];

		public int Length
		{
			[MethodImpl(MethodImplOptions.AggressiveInlining)]
			get => array.Length;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public IEnumerator<T> GetEnumerator() => (array as IEnumerable<T>).GetEnumerator();


		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		IEnumerator IEnumerable.GetEnumerator() => array.GetEnumerator();
	}



	public sealed class ObjectPool<T> : IEnumerable<T> where T : Component
	{
		private readonly List<T> used = new List<T>(), free = new List<T>();
		private readonly T prefab;
		private readonly Transform anchor;


		/// <param name="anchor">Anchor để gắn các <see cref="T"/> object</param>
		public ObjectPool(T prefab, Transform anchor = null)
		{
			this.prefab = prefab;
			this.anchor = anchor;
		}


		/// <param name="active">Active gameObject ngay lập tức ?</param>
		public T Get(Vector3 position = default, bool active = true)
		{
			T obj;
			if (free.Count != 0)
			{
				obj = free[0];
				free.RemoveAt(0);
			}
			else obj = UnityEngine.Object.Instantiate(prefab, anchor);

			used.Add(obj);
			obj.transform.position = position;
			obj.gameObject.SetActive(active);
			return obj;
		}


		public void Recycle(T obj)
		{
			obj.gameObject.SetActive(false);
			used.Remove(obj);
			free.Add(obj);
		}


		public void Recycle()
		{
			for (int i = 0; i < used.Count; ++i)
			{
				var obj = used[i];
				obj.gameObject.SetActive(false);
				free.Add(obj);
			}
			used.Clear();
		}


		IEnumerator IEnumerable.GetEnumerator() => used.GetEnumerator();


		public IEnumerator<T> GetEnumerator() => (used as IEnumerable<T>).GetEnumerator();
	}



	public sealed class SystemObjectPool<T> where T : class, new()
	{
		private readonly List<T> free = new List<T>(), used = new List<T>();


#if DEBUG
		public SystemObjectPool()
		{
			if (typeof(UnityEngine.Object).IsAssignableFrom(typeof(T)))
				throw new Exception("Nên sử dụng ObjectPool cho Unity obj!");
		}
#endif


		public T Get()
		{
			T obj;
			if (free.Count != 0)
			{
				obj = free[0];
				free.RemoveAt(0);
			}
			else obj = new T();
			used.Add(obj);
			return obj;
		}


		public void Recycle(T obj)
		{
			used.Remove(obj);
			free.Add(obj);
		}


		public void Recycle()
		{
			for (int i = 0; i < used.Count; ++i) free.Add(used[i]);
			used.Clear();
		}
	}



	public sealed class CustomCancelSource : IDisposable
	{
		#region Static
		private static readonly CancellationTokenSource DISPOSED_CTS = new CancellationTokenSource();
		private static readonly List<CustomCancelSource> sources = new List<CustomCancelSource>();


		static CustomCancelSource()
		{
			DISPOSED_CTS.Dispose();
			SceneManager.sceneUnloaded += (scene) =>
			  {
				  if (!scene.isLoaded) return;
				  foreach (var source in sources)
				  {
					  source.cts?.Dispose();
					  source.cts = DISPOSED_CTS;
				  }
				  sources.Clear();
			  };
		}
		#endregion


		private CancellationTokenSource cts;
		public CancellationToken Token => cts.Token;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public CustomCancelSource(params CancellationToken[] tokens)
		{
			cts = tokens.Length != 0 ? CancellationTokenSource.CreateLinkedTokenSource(tokens) : new CancellationTokenSource();
			sources.Add(this);
		}


		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Reset(params CancellationToken[] tokens)
		{
			cts?.Dispose();
			cts = tokens.Length != 0 ? CancellationTokenSource.CreateLinkedTokenSource(tokens) : new CancellationTokenSource();
		}


		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void Cancel()
		{
			cts.Cancel();
			cts = null;
		}


		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public void CancelAndReset(params CancellationToken[] tokens)
		{
			cts.Cancel();
			Reset(tokens);
		}


		public void Dispose()
		{
			cts?.Dispose();
			cts = DISPOSED_CTS;
			sources.Remove(this);
		}
	}



	public static class Prefab
	{
		private static readonly Dictionary<string, UnityEngine.Object> prefabs = new Dictionary<string, UnityEngine.Object>();

		public static T Load<T>(string postFix = "") where T : UnityEngine.Object
		{
			string key = $"{typeof(T)}{postFix}";
			if (prefabs.TryGetValue(key, out UnityEngine.Object prefab)) return prefab as T;
			return (prefabs[key] = Resources.Load<T>(key)) as T;
		}
	}
}